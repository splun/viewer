
function loadAuthMSG(){
	$("#addQuotes,#viewBookmarks,#viewQuotes,#addBookmarks,#viewComments,#addComments").attr("disabled","disabled");

  	$("#addQuotes,#viewBookmarks,#viewQuotes,#addBookmarks,#viewComments,#addComments").click(function(){
		$.magnificPopup.open({
		  items: {
		    src: '<div id="logginMessage">Для работы с закладками, цитатами и комментариями вам необходимо <a target="_blank" href="https://bookonlime.ru/user">авторизироваться</a>.</div>', // can be a HTML string, jQuery object, or CSS selector
		    type: 'inline'
		  }
		});
	});
}

function loadList(method){

	var args = {
		m: "get", 
		t: method,
		user: $("#uid").val(),
		book: $("#bid").val()	
	};

	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	if(result.status=="success"){
	    		var data = result.data;
	    		var html = "";
	    		$.each(data, function(index, val){
	    			if(args.t == "Comments"){
	      				html += "<li data-item='"+val.id+"'><a class='item-link' href='"+val.link+"'>"+val.title+"</a><span class='anch-text hidden'>"+val.seltext+"</span><a class='delete'></a><a class='edit-link'></a></li>";
	    			}else{
	    				html += "<li data-item='"+val.id+"'><a class='item-link' href='"+val.link+"'>"+val.title+"</a><a class='delete'></a><a class='edit-link'></a></li>";
	    			}
	    		});
				$("ul#"+method).html(html);
	    	}else{
	    		alert(result.reason);
	    	}
	    }
	});  
}

function getSelectedText(){
    var text = "";
    if (window.getSelection) {
        text = window.getSelection();
    }else if (document.getSelection) {
        text = document.getSelection();
    }else if (document.selection) {
        text = document.selection.createRange().text;
    }
    return text;
}

function getSelectionHtml(){
	var html = "";
	if(typeof window.getSelection != "undefined"){
		var sel = window.getSelection();
		if(sel.rangeCount){
			var container = document.createElement("div");
			for(var i = 0, len = sel.rangeCount; i < len; ++i){
				container.appendChild(sel.getRangeAt(i).cloneContents());
			}
			html = container.innerHTML;
		}
	}else if(typeof document.selection != "undefined"){
		if(document.selection.type == "Text"){
			html = document.selection.createRange().htmlText;
		}
	};
	return html;
}


function itemAdd(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	      if(result.status == "success"){
	      	if(args.t == "Comments"){
				$("ul#"+args.t).append("<li data-item='"+result.id+"'><a class='item-link' href='"+args.link+"'>"+args.title+"</a><span class='anch-text hidden'>"+args.seltext+"</span><a class='delete'></a><a class='edit-link'></a></li>");
	      	}else{
	      		$("ul#"+args.t).append("<li data-item='"+result.id+"'><a class='item-link' href='"+args.link+"'>"+args.title+"</a><a class='delete'></a><a class='edit-link'></a></li>");
	      	}
	      }else{
	      	console.log(result.status);
	      }
	      setTimeout(function(){
	      	$("#"+args.m+args.t).removeAttr("disabled");
	      },800);
	    }
	});  
}

function itemEdit(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	console.log(result);
	    	if(result.status=="success"){
	    		var el = $("ul#"+args.t).find($("li[data-item='"+args.id+"']"));

				$(el).find(".item-link").text(args.title).show();
					if(args.t == "Bookmarks"){
						$(el).find("input.bookmark-edit-input").remove();
					}
					if(args.t == "Comments"){
						$(el).find("textarea.comment-textarea").remove();
					}
					$(el).find("a.save-button").removeClass("save-button").addClass("edit-link");
	    	}else{
	    		alert(result.status);
	    		if(args.t == "Bookmarks"){
	    			$(el).find("input.bookmark-edit-input").focus();
	    		}
	    		if(args.t == "Comments"){
	    			$(el).find("textarea.comment-textarea").focus();
				}
	    	}
	    }
	});  
}

function itemDelete(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	if(result.status=="success"){
				$("ul#"+args.t).find($("li[data-item='"+args.id+"']")).remove();
	    	}else{
	    		console.log(result.reason);
	    	}
	    }
	});  
}

$("document").ready(function(){

	$("#Home").attr("href","/node/"+$("#bid").val()+"");

	$("#addBookmarks").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");

		var title = "Закладка";
		var page = $(this).attr("href").split("&");

		if(page!==""){
			title = "Закладка на странице "+page[0].replace("#page=","");
		} 

		var args = {
			m: "add", 
			t: "Bookmarks",
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: title,
			link: $(this).attr("href")
		};

		$("#viewBookmarks").trigger("click");
		itemAdd(args); 
		return false;
	});


    $("body,html").on("click","#Bookmarks li a.edit-link",function(){

   		var parent = $(this).parent();
   		var title = $(parent).find(".item-link").text();
   		var input = '<input type="text"  value="'+title+'" name="bookmark-title" class="bookmark-edit-input"></input>';
   		
   		$(parent).find(".item-link").hide();
   		$(parent).prepend(input);
   		$(this).parent().find(".bookmark-edit-input").focus();
   		$(this).removeClass("edit-link").addClass("save-button");
   		return false;
   });


   $("body,html").on("click","#Bookmarks li a.save-button",function(){
   		var parent = $(this).parent();
   		var newtitle = $(parent).find(".bookmark-edit-input").val();
   		var id = $(this).parent().attr("data-item");

   		if(newtitle == ""){
   			alert("Пожалуйста введите название закладки!");
   			$(parent).find(".bookmark-edit-input").focus();
   			return false
   		}

   		var args = {
			m: "edit", 
			t: "Bookmarks",
			id: id,
			title: newtitle
		};

		itemEdit(args);

		/*$(parent).find(".item-link").text(newtitle).show();
   		$(parent).find(".bookmark-edit-input").remove();
   		$(this).removeClass("save-button").addClass("edit-link");*/

   		return false;
    });

	$("#addQuotes").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");

		var selectedText = getSelectedText();

		//сразу Text присутствует проблема слепливания слов при переносе
		//var quote = selectedText.toString(); 

		var quote = getSelectionHtml().replace(/<[^>]+>/g,' ').replace( /\s+/g,' ').replace("- ","").trim(""); 

		if(!quote || quote == ""){

			alert("Пожалуйста выделите интересущую вас часть текста");

			$(this).removeAttr("disabled");

			return false;
		}else{

			if (quote.length >= 150) {
			    quote = quote.substring(0, 147);
			    var lastIndex = quote.lastIndexOf(" ");       
			    quote = quote.substring(0, lastIndex) + '...'; 
			}

		}

		var args = {
			m: "add", 
			t: "Quotes", 
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: quote,
			link: $("#addBookmarks").attr("href")
		};
		$("#viewQuotes").trigger("click");
		itemAdd(args);
		return false;
	});

	$("body,html").on("click","#Quotes li a.item-link",function(){
		var title = $(this).text().split(" ");
		anch = "";
		for(i=0;i<=1;i++){
			if(title[i]){
				anch += title[i]+" ";
			}
		}
	
		//$('.textLayer span.highlight').replace(/<[^>]+>/g,' ');
		$('img.quote-icon').remove();

  		setTimeout(function(){
	  		$('.textLayer').each(function(){
	   		 jQuery(this).html(jQuery(this).html().replace(anch, '<img src="assets/images/quotes-png-99.png" class="quote-icon"></i>$&')); //<span class="highlight"></span> выделяем найденные фрагменты
	   		 });
  		},700);

	});

	$("#addComments").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");

		var selectedText = getSelectedText();

		if(!selectedText || selectedText == ""){

			alert("Пожалуйста выделите интересущую вас строку или абзац");
			$(this).removeAttr("disabled");

			return false;
		}

		var seltext = getSelectionHtml().replace(/<[^>]+>/g,' ').replace( /\s+/g,' ').replace("- ","").trim(""); 

		if (seltext.length >= 150) {
		    seltext = seltext.substring(0, 147);
		    var lastIndex = seltext.lastIndexOf(" ");       
		    seltext = seltext.substring(0, lastIndex) + '...'; 
		}

		var link = $("#addBookmarks").attr("href");
		$("#viewComments").trigger("click");
		$("#Comments").append('<li><textarea class="comment-textarea" name="comment" placeholder="Введите текст комментария"></textarea><input type="hidden" name="comment-link" class="comment-link" value="'+link+'"><input type="hidden" class="sel-text" value="'+seltext+'"></input><a class="close"></a><a class="save-button new-comment"></a></li>');
		$("#Comments .comment-textarea").focus();
		return false;

	});


	$("body,html").on("click","#Comments .new-comment",function(){
		var comment = $(this).parent().find(".comment-textarea").val();
		var link = $(this).parent().find(".comment-link").val();
		var seltext = $(this).parent().find(".sel-text").val();

		if(comment == ""){
			alert("Пожалйуста введите свой комментарий");
			$(this).parent().find(".comment-textarea").focus();
			return false;
		}

		var args = {	
			m: "add", 
			t: "Comments", 
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: comment,
			link: link,
			seltext:seltext
		};

		itemAdd(args);
		$(this).parent().remove();
		return false;
	});


	$("body,html").on("click","#Comments .close",function(){
		$(this).parent().remove();
		$("#addComments").removeAttr("disabled");
	});

    $("body,html").on("click","#Comments li a.edit-link",function(){

   		var parent = $(this).parent();
   		var title = $(parent).find(".item-link").text();
   		var input = '<textarea class="comment-textarea" name="comment">'+title+'</textarea>';
   		
   		$(parent).find(".item-link").hide();
   		$(parent).prepend(input);
   		$(this).parent().find(".comment-textarea").focus();
   		$(this).removeClass("edit-link").addClass("save-button");
   		return false;
   });

   $("body,html").on("click","#Comments li a.save-button",function(){
   		if($(this).hasClass("new-comment")) return false;
   		var parent = $(this).parent();
   		var newtitle = $(parent).find(".comment-textarea").val();
   		var id = $(this).parent().attr("data-item");

   		if(newtitle == ""){
   			alert("Пожалуйста введите коментарий!");
   			$(parent).find(".comment-textarea").focus();
   			return false
   		}

   		var args = {
			m: "edit", 
			t: "Comments",
			id: id,
			title: newtitle
		};

		itemEdit(args);

		/*$(parent).find(".item-link").text(newtitle).show();
   		$(parent).find(".bookmark-edit-input").remove();
   		$(this).removeClass("save-button").addClass("edit-link");*/

   		return false;
    });

   	$("body,html").on("click","#Comments li a.item-link",function(){
		var anchtext = $(this).parent().find(".anch-text").text().split(" ");
		
		anch = "";
		for(i=0;i<=1;i++){
			if(anchtext[i]){
				anch += anchtext[i]+" ";
			}
		}
		//$('.textLayer span.highlight').replace(/<[^>]+>/g,' ');
		//$('.comments-icon').remove();

  		setTimeout(function(){
	  		$('.textLayer').each(function(){
	   		 jQuery(this).html(jQuery(this).html().replace(anch, '<img src="assets/images/comment.png" class="comments-icon">')); //<span class="highlight"></span> выделяем найденные фрагменты
	   		 });
  		},1000);

	});

	$("body,html").on("click",".delete",function(){
		var id = $(this).parent().attr("data-item");
		var table = $(this).parent().parent().attr("id");

		var args = {
			m: "delete", 
			t: table,
			//user: $("#uid").val(),
			book: $("#bid").val(),
			id: id
		};

		itemDelete(args);
		return false;
	});

   $("body,html").on("click","#spreadEven",function(){

   	if(!$(this).hasClass("active")){ // Включаем книжный вид
   		window.location.hash="";
   		var col_count = 2;
		var $c = $('#viewer');
		while($c.children('.page:not(:first):not(:last)').length){
			$c.children('.page:not(:first):not(:last):lt('+col_count+')').wrapAll('<div class="spread">');	
		}
		window.location.hash="zoom=90";
   		//$("#viewer .page").slice(2,3).wrapAll('<div class="spread"></div>');
   		$(this).addClass("active");
   		return false;
   	}else{ // Отключаем книжный вид
   		$(".spread .page").unwrap(".spread");
   		$(this).removeClass("active");
   		return false;
   	}

   });
	
	$("body,html").on("mouseover",".delete",function(){
		$(this).parent().find('.item-link').addClass("light");
	});
	$("body,html").on("mouseout",".delete",function(){
		$(this).parent().find('.item-link').removeClass("light");
	});

	$(document).bind('keydown', function(e) {
	  if(e.ctrlKey && (e.which == 83)) {
	    e.preventDefault();
	    return false;
	  }
	});

  document.oncontextmenu = function() {
        //return false;
   };

});