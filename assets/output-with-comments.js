/*******Проверяем пользователя ***********/
function loadAuthMSG(){
	$("#addQuotes,#viewBookmarks,#viewQuotes,#addBookmarks,#viewComments,#addComments").attr("disabled","disabled");

  	$("#addQuotes,#viewBookmarks,#viewQuotes,#addBookmarks,#viewComments,#addComments").click(function(){
		$.magnificPopup.open({
		  items: {
		    src: '<div id="logginMessage">Для работы с закладками, цитатами и комментариями вам необходимо <a target="_blank" href="https://bookonlime.ru/user">авторизироваться</a>.</div>', // can be a HTML string, jQuery object, or CSS selector
		    type: 'inline'
		  }
		});
	});
}

/*******Получаем данные из базы и загружаем списки */
function loadList(method){

	var args = {
		m: "get", 
		t: method,
		user: $("#uid").val(),
		book: $("#bid").val()	
	};

	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	if(result.status=="success"){ //В случае успеха пербираем массив и выводим список
	    		var data = result.data;
	    		var html = "";
	    		$.each(data, function(index, val){
	      			html += "<li data-item='"+val.id+"'><a class='item-link' href='"+val.link+"'>"+val.title+"</a><a class='delete'></a><a class='edit-link'></a></li>";
	    		});
				$("ul#"+method).html(html); //вывод списка
	    	}else{
	    		alert(result.reason); //В случае ошибки вывод причину из ответа
	    	}
	    }
	});  
}

/*********Метод выделения чистого текста**************/
function getSelectedText(){
    var text = "";
    if (window.getSelection) {
        text = window.getSelection();
    }else if (document.getSelection) {
        text = document.getSelection();
    }else if (document.selection) {
        text = document.selection.createRange().text;
    }
    return text;
}

/*********Метод выделения куска html**************/
function getSelectionHtml(){
	var html = "";
	if(typeof window.getSelection != "undefined"){
		var sel = window.getSelection();
		if(sel.rangeCount){
			var container = document.createElement("div");
			for(var i = 0, len = sel.rangeCount; i < len; ++i){
				container.appendChild(sel.getRangeAt(i).cloneContents());
			}
			html = container.innerHTML;
		}
	}else if(typeof document.selection != "undefined"){
		if(document.selection.type == "Text"){
			html = document.selection.createRange().htmlText;
		}
	};
	return html;
}

/*Общая функция для добавления записей, Ajax запрос на файл app/requests.php 
с параметрами args */
function itemAdd(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	      if(result.status == "success"){ //В случае успеха добавляем строку к текущему списку

			$("ul#"+args.t).append("<li data-item='"+result.id+"'><a class='item-link' href='"+args.link+"'>"+args.title+"</a><a class='delete'></a><a class='edit-link'></a></li>");
	      
	      }else{ //В случае ошибки debug
	      	console.log(result.status);
	      }
	      setTimeout(function(){
	      	$("#"+args.m+args.t).removeAttr("disabled");
	      },800);
	    }
	});  
}

/*Общая функция для редактирования записей, Ajax запрос на файл app/requests.php 
с параметрами args */
function itemEdit(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	console.log(result);
	    	if(result.status=="success"){
	    		var el = $("ul#"+args.t).find($("li[data-item='"+args.id+"']"));

				$(el).find(".item-link").text(args.title).show();
					$(el).find("input.bookmark-edit-input").remove();
					$(el).find("a.save-button").removeClass("save-button").addClass("edit-link");
	    	}else{
	    		alert(result.status);
	    		$(el).find("input.bookmark-edit-input").focus();
	    	}
	    }
	});  
}

/*Общая функция для удаления записей, Ajax запрос на файл app/requests.php 
с параметрами args */
function itemDelete(args){
	$.ajax({
	    type: "POST",
	    data: {
	    	args
	    },
	    url: "app/requests.php",
	    dataType: "json",
	    beforeSend: function() {
	    },
	    async: false,
	    success: function(result) {
	    	if(result.status=="success"){ //В случае успеха удаляем соответствующую строку
				$("ul#"+args.t).find($("li[data-item='"+args.id+"']")).remove();
	    	}else{ // В случае ошибки debug
	    		console.log(result.reason); 
	    	}
	    }
	});  
}

$("document").ready(function(){

	$("#Home").attr("href","/node/"+$("#bid").val()+"");

	$("#addBookmarks").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");
		
		/* Собираем заголовок закладке с получением номера страницы*/
		var title = "Закладка";
		var page = $(this).attr("href").split("&");

		if(page!==""){
			title = "Закладка на странице "+page[0].replace("#page=","");
		} 

		var args = { //параметры для передачи средствами Ajax
			m: "add", 
			t: "Bookmarks",
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: title,
			link: $(this).attr("href")
		};

		$("#viewBookmarks").trigger("click"); //Открываем вкладку закладок
		itemAdd(args); //Вызываем общую функцию для добавления закладки, с парметрами args 
		return false;
	});

	// Создаем поле для ввода нового заголовка для закладки
    $("body,html").on("click","#Bookmarks li a.edit-link",function(){

   		var parent = $(this).parent();
   		var title = $(parent).find(".item-link").text();
   		var input = '<input type="text"  value="'+title+'" name="bookmark-title" class="bookmark-edit-input"></input>';
   		
   		$(parent).find(".item-link").hide();
   		$(parent).prepend(input);
   		$(this).parent().find(".bookmark-edit-input").focus();
   		$(this).removeClass("edit-link").addClass("save-button");
   		return false;
   });

   // Сохраняем новый заголовок закладки и возвращаем прежний вид
   $("body,html").on("click","#Bookmarks li a.save-button",function(){
   		var parent = $(this).parent();
   		var newtitle = $(parent).find(".bookmark-edit-input").val();
   		var id = $(this).parent().attr("data-item");

   		if(newtitle == ""){
   			alert("Пожалуйста введите название закладки!");
   			$(parent).find(".bookmark-edit-input").focus();
   			return false;
   		}

   		var args = {
			m: "edit", 
			t: "Bookmarks",
			id: id,
			title: newtitle
		};

		itemEdit(args);

		/*$(parent).find(".item-link").text(newtitle).show();
   		$(parent).find(".bookmark-edit-input").remove();
   		$(this).removeClass("save-button").addClass("edit-link");*/

   		return false;
    });

	$("#addQuotes").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");

		var selectedText = getSelectedText();

		//сразу Text присутствует проблема слепливания слов при переносе
		//var quote = selectedText.toString(); 

		//Html и обрезаем теги , решаем проблему с слепливанием слов при переносе
		var quote = getSelectionHtml().replace(/<[^>]+>/g,' ').replace( /\s+/g,' ').replace("- ","").trim(""); 

		if(!quote || quote == ""){

			alert("Пожалуйста выделите интересущую вас часть текста");

			$(this).removeAttr("disabled");

			return false;
		}else{
			//обрезаем до нужной величины
			if (quote.length >= 150) {
			    quote = quote.substring(0, 147);
			    var lastIndex = quote.lastIndexOf(" ");       
			    quote = quote.substring(0, lastIndex) + '...'; 
			}

		}

		var args = {
			m: "add", 
			t: "Quotes", 
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: quote,
			link: $("#addBookmarks").attr("href")
		};
		$("#viewQuotes").trigger("click");
		itemAdd(args); //Вызываем общую функцию для добавления закладки, с параметрами args
		return false;
	});

	$("body,html").on("click","#Quotes li a.item-link",function(){
		var title = $(this).text().split(" ");
		anch = "";
		for(i=0;i<=1;i++){
			if(title[i]){
				anch += title[i]+" ";
			}
		}
	
		//$('.textLayer span.highlight').replace(/<[^>]+>/g,' ');
		$('img.quote-icon').remove(); // удаляем другие метки

  		setTimeout(function(){
	  		$('.textLayer').each(function(){ // в селекторе задаем область поиска
	   		 jQuery(this).html(jQuery(this).html().replace(anch, '<img src="assets/images/quotes-png-99.png" class="quote-icon"></i>$&')); //<span class="highlight"></span> выделяем найденные фрагменты
	   		 });
  		},700);

	})

    /*** Создаем ссылку на выделенную область и поле для ввода комментария***/
   	$("#addComments").click(function(){

		if($(this).attr("disabled")) {return false;}

		$(this).attr("disabled","disabled");

		var selectedText = getSelectedText();

		if(!selectedText || selectedText == ""){

			alert("Пожалуйста выделите интересущую вас часть текста");

			$(this).removeAttr("disabled");

			return false;
		}

		var link = $("#addBookmarks").attr("href");
		$("#viewComments").trigger("click");
		$("#Comments").append('<li><textarea class="comment-textarea" name="comment" placeholder="Введите текст комментария"></textarea><input type="hidden" name="comment-link" class="comment-link" value="'+link+'"><a class="close"></a><a class="save-button new-comment"></a></li>');
		$("#Comments .comment-textarea").focus();
		return false;

	});

   	/*********Сохраняем комментарий***********/
	$("body,html").on("click","#Comments .new-comment",function(){
		var comment = $(this).parent().find(".comment-textarea").val();
		var link = $(this).parent().find(".comment-link").val();

		if(comment == ""){
			alert("Пожалйуста введите свой комментарий");
			$(this).parent().find(".comment-textarea").focus();
			return false;
		}

		var args = {	
			m: "add", 
			t: "Comments", 
			//user: $("#uid").val(),
			book: $("#bid").val(),
			title: comment,
			link: link
		};

		itemAdd(args);
		$(this).parent().remove();
		return false;
	});

	/*******Отменяем создание комментария, удаляем строку*******/
	$("body,html").on("click","#Comments .close",function(){
		$(this).parent().remove();
	});


	/********Вызов функции для удаления соответствующей строки**********/
	$("body,html").on("click",".delete",function(){
		var id = $(this).parent().attr("data-item");
		var table = $(this).parent().parent().attr("id");

		var args = {
			m: "delete", 
			t: table,
			//user: $("#uid").val(),
			book: $("#bid").val(),
			id: id
		};

		itemDelete(args);
		return false;
	});


   // Книжный вид 1 и последняя страница одиночные
   $("body,html").on("click","#spreadEven",function(){

   	if(!$(this).hasClass("active")){ // Включаем книжный вид
   		window.location.hash="";
   		var col_count = 2;
		var $c = $('#viewer');
		while($c.children('.page:not(:first):not(:last)').length){
			$c.children('.page:not(:first):not(:last):lt('+col_count+')').wrapAll('<div class="spread">');	
		}
		window.location.hash="zoom=90";
   		//$("#viewer .page").slice(2,3).wrapAll('<div class="spread"></div>');
   		$(this).addClass("active");
   		return false;
   	}else{ // Отключаем книжный вид
   		$(".spread .page").unwrap(".spread");
   		$(this).removeClass("active");
   		return false;
   	}

   });
	
	$("body,html").on("mouseover",".delete",function(){
		$(this).parent().find('.item-link').addClass("light");
	});
	$("body,html").on("mouseout",".delete",function(){
		$(this).parent().find('.item-link').removeClass("light");
	});

	//Запрещаем комбинацию клавиш ctrl+s
	$(document).bind('keydown', function(e) {
	  if(e.ctrlKey && (e.which == 83)) {
	    e.preventDefault();
	    return false;
	  }
	});

	//Запрещаем клик правой кнопкой и вызов контекстного меню
  	document.oncontextmenu = function() {
        return false;
   	};

});