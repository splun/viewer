<?php 
require_once("config.php");
class DB{

	protected $connection;

    public function __construct() {

        $this->connection = new mysqli(HOST, USER, PASSWORD, DB);

        if( !$this->connection ) {
            throw new Exception('Could not connect to DB ');
        }
    }

    public function query($sql){

	    if ( !$this->connection ){
	        return false;
	    }

	    $result = $this->connection->query($sql);

	    if ( mysqli_error($this->connection) ){
	       throw new Exception(mysqli_error($this->connection));
	    }

	    if ( is_bool($result) ){
	        return $result;
	    }

	    $data = array();
	    while( $row = mysqli_fetch_assoc($result) ){
	        $data[] = $row;
	    }

	    mysqli_free_result($result);

	    return $data;
    }
}
