<?php if(!isset($_GET['user']) || !isset($_GET['book'])){?>
    <h2 style="text-align:center;margin-top:20%;">PAGE NOT FOUND</h2>
<?php 

die();

}?>

<html lang="ru">
  <head>
    <title>PDF.js viewer</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="../assets/output.js"></script>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>

    <h2>Bookmarks</h2>
    <a href="#page=1&zoom=auto,-214,188" id="addBookmarks">Add Bookmarks</a><br />
    <ul id="Bookmarks"></ul>
    
    <h2>Quotes</h2>
    <a href="#page=1&zoom=auto,-214,188" id="addQuotes">Add Quotes</a>
    <ul id="Quotes"></ul>

    <input type="hidden" id="uid" name="uid" value="<?php echo $_GET['user'];?>" />
    <input type="hidden" id="bid" name="bid" value="<?php echo $_GET['book'];?>" />

  </body>
</html>
