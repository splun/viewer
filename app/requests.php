<?php
    session_start(); 
	require_once("model.php"); //Подключаем модель с методами для работы с базой

	$args = $_REQUEST['args']; 
    
    if(isset($args) && isset($args['m']) && function_exists($args['m']))
    {
    	// Обьявляем класс и вызываем функцию переданную в $args['m'] 

    	$model = new Model;  

        $response = call_user_func_array($args['m'],array($args,$model));
    }

    /********Добавление закладок или цитат************/
    function add($args,$model){

        //достаем ID пользователя из текущей сессии
        if(isset($_SESSION['user']) && (int)$_SESSION['user']){ 
            $user = $_SESSION['user'];
        }else{
            die(json_encode(array("status"=>"error","reason"=>"Incorect User Id")));
        }

    	//передаем ID книги
    	if(isset($args['book']) && (int)$args['book']){
    		$book = $args['book'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect Book Id")));
    	}

    	//передаем название таблицы в которую записываем (bookmarks либо quotes)
    	if(isset($args['t']) && ($args['t'] == "Bookmarks" || $args['t'] == "Quotes" || $args['t'] == "Comments")){ 
    		$tbl = $args['t'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect query")));
    	}

    	//передаем ссылку на закладку или цитату
		if(isset($args['link']) && !empty($args['link'])){
    		$link = $args['link'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect Link"))); 
    	}

        if($args['t'] == "Comments" && isset($args['seltext']) && !empty($args['seltext'])){
            $seltext = $args['seltext'];
        }else{
            $seltext = "";
        }

    	//передаем название закладки или цитаты
    	if(isset($args['title']) && !empty($args['title'])){

            $title = $args['title'];

            /* $title = strip_tags(stripslashes($args['title']));
            if(strlen($title) >= 150){
                $title = substr($args['title'], 0, 147)."...";
            }*/

    	}else{
    		if($args['t'] == "Bookmarks"){
    			$title = "Bookmark";
    		}
    		if($args['t'] == "Quotes"){
				$title = "Quote";
    		}
    	}

        //Вызов модели для запроса на добавление закладки,цитат и комментариев
    	$data = $model->add($user,$book,$title,$link,$tbl,$seltext); 

    	echo json_encode(array("status"=>"success","id"=>$data));
    }

    function edit($args,$model){

        //достаем ID пользователя из текущей сессии
        if(isset($_SESSION['user']) && (int)$_SESSION['user']){ 
            $user = $_SESSION['user'];
        }else{
            die(json_encode(array("status"=>"error","reason"=>"Incorect User Id")));
        }

        //передаем ID закладки или комментария
        if(isset($args['id']) && (int)$args['id']){
            $id = $args['id'];
        }else{
            die(json_encode(array("status"=>"error","reason"=>"Incorect Id")));
        }

        //передаем название таблицы)
        if(isset($args['t']) && ($args['t'] == "Bookmarks" || $args['t'] == "Quotes" || $args['t'] == "Comments")){ 
            $tbl = $args['t'];
        }else{
            die(json_encode(array("status"=>"error","reason"=>"Incorect query")));
        }

        //передаем название закладки или цитаты
        if(isset($args['title']) && !empty($args['title'])){

            $title = $args['title'];

            /* $title = strip_tags(stripslashes($args['title']));
            if(strlen($title) >= 150){
                $title = substr($args['title'], 0, 147)."...";
            }*/

        }else{
            if($args['t'] == "Bookmarks"){
                $title = "Bookmark";
            }
            if($args['t'] == "Quotes"){
                $title = "Quote";
            }
        }

        $data = $model->update($user,$id,$title,$tbl); //Вызов модели для запроса на редактирование закладки

        echo json_encode(array("status"=>$data));
    }

    /********Получение закладок или цитат************/
    function get($args,$model){

    	//передаем название таблицы из которой удаляем (bookmarks либо qoutes)
    	if(isset($args['t']) && ($args['t'] == "Bookmarks" || $args['t'] == "Quotes" || $args['t'] == "Comments")){ 
    		$tbl = $args['t'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect query")));
    	}

        //передаем ID пользователя
        if(isset($args['user']) && (int)$args['user']){ //Если передаем параметром(для внешних запросов)
            $user = $args['user'];
        }else{
            //Если нет парметра достаем из текущей сессии пользователя
            if(isset($_SESSION['user']) && (int)$_SESSION['user']){ 
                $user = $_SESSION['user'];
            }else{
                die(json_encode(array("status"=>"error","reason"=>"Incorect User Id")));
            }
        }

    	//передаем ID книги
    	if(isset($args['book']) && (int)$args['book']){
    		$book = $args['book'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect Book Id")));
    	}

		$data = $model->get($user,$book,$tbl); //Вызов модели для запроса на получение закладок или цитат

    	echo json_encode(array("status"=>"success","data"=>$data));
    }


    /********Удаление закладок или цитат************/
    function delete($args,$model){
		
		//передаем название таблицы из которой удаляем (bookmarks либо qoutes)
    	if(isset($args['t']) && ($args['t'] == "Bookmarks" || $args['t'] == "Quotes" || $args['t'] == "Comments")){ 
    		$tbl = $args['t'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect query")));
    	}

    	if(isset($args['id']) && (int)$args['id']){
    		$id = $args['id'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect Id")));
    	}

    	//достаем ID пользователя из текущей сессии
        if(isset($_SESSION['user']) && (int)$_SESSION['user']){
            $user = $_SESSION['user'];
        }else{
            die(json_encode(array("status"=>"error","reason"=>"Incorect User Id")));
        }

    	//передаем ID Книги
    	if(isset($args['book']) && (int)$args['book']){
    		$book = $args['book'];
    	}else{
    		die(json_encode(array("status"=>"error","reason"=>"Incorect Book Id")));
    	}


		$data = $model->delete($user,$book,$tbl,$id); //Вызов модели для запроса на удаление записей

    	echo json_encode(array("status"=>$data));
    }
?>