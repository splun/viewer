<?php 
//File: /viewer/app/model.php

require_once("db.class.php"); 


class Model extends DB{

    //Метод получения данных из db
    public function get($user,$book,$tbl){

        if(!$user || !$book || !$tbl) return false;

        /*Собираем название таблицы для запроса, убираем из строки лишнее.
        Добавляем pdf_ префикс таблицы */
        $tbl = "pdf_".strip_tags(stripslashes(strtolower($tbl)));

        //Для выполнения запроса используем метод класса DB - query
        $data = $this->query("SELECT * FROM `$tbl` WHERE `user_id` = $user AND `book_id` = $book");

        return $data; 
    }


    //Метод отправки данных в bd
    public function add($user, $book, $title, $link, $tbl, $seltext = ""){

        if(!$user || !$book || !$link || !$tbl ) return false;

        $link = strip_tags(stripslashes($link));
        $title = strip_tags(stripslashes($title));
        $tbl = "pdf_".strip_tags(stripslashes(strtolower($tbl)));


        if($seltext !== "" && $tbl == "pdf_comments"){

            $query = $this->query("INSERT INTO `$tbl`(`user_id`,`book_id`,`title`,`link`,`seltext`) VALUES ($user, $book, '{$title}', '{$link}', '{$seltext}')");
        }else{

            $query = $this->query("INSERT INTO `$tbl`(`user_id`,`book_id`,`title`,`link`) VALUES ($user, $book, '{$title}', '{$link}')");  
        }


        if($query){
            return $this->connection->insert_id; //Возвращаем ID добавленной записи
        }else{
            return "error";
        }
    }

    //Метод редактирования данных в bd
    public function update($user, $id, $title, $tbl){
        
        if(!$user || !$id || !$title || !$tbl ) return false;

        $title = strip_tags(stripslashes($title));
        $tbl = "pdf_".strip_tags(stripslashes(strtolower($tbl)));

        $query = $this->query("UPDATE `$tbl` SET `title` = '{$title}' WHERE `user_id` = $user AND `id` = $id");

        if($query){
            return "success";
        }else{
            return "error";
        }
    }
    //Метод удаления данных из bd
    public function delete($user, $book, $tbl, $id){
        if(!$user || !$book || !$tbl || !$id) return;

        $tbl = "pdf_".strip_tags(stripslashes(strtolower($tbl)));
        $query = $this->query("DELETE FROM `$tbl` WHERE `user_id` = $user AND `book_id` = $book AND `id` = $id");

        if($query){
            return "success";
        }else{
            return "error";
        }
    }

}